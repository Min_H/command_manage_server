package com.suitbim.commandserver.commandserver.client.ifs;

import java.io.File;

public interface CommandClient {

    String getClientPath();

    String getInputPath();

    File currentFile(String command);

    File currentFile(Command command);

    String buildCommand(Command command);

    boolean execute(Command command);

    boolean execute(String command);

    class Command {
        String source;
        String biz;
        String destination;
        String option;
        Param param;

        public Command() {
            this.param = new Param();
        }

        public String getDestination() {
            return destination;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public void setBiz(String biz) {
            this.biz = biz;
        }

        public void setOption(String option) {
            this.option = option;
        }

        public void setParam(Param param) {
            this.param = param;
        }

        public Long getOptionId() {
            return param.getOptionId();
        }

        public String getAccount() {
            return param.getAccount();
        }

        public String getPassword() {
            return param.getPassword();
        }

        public Long getProjectId() {
            return param.getProjectId();
        }

        public Long getSceneId() {
            return param.getSceneId();
        }

        public void setSceneId(Long sceneId) {
            param.setSceneId(sceneId);
        }

        public String getDescription() {
            return param.getDescription();
        }

        public String getName() {
            return param.getName();
        }

        public boolean isAddMode() {
            return param.isAddMode();
        }

        public boolean isUploadProperty() {
            return param.isUploadProperty();
        }

        public String getUrl() {
            return param.getUrl();
        }

        public Param getParam() {
            return param;
        }

        public class Param {
            Long optionId;
            String account;
            String password;
            Long projectId;
            Long sceneId;
            Integer version;
            Integer lodLevel;
            String description;
            String name;
            boolean addMode;
            boolean uploadProperty;
            String url;

            public Long getOptionId() {
                return optionId;
            }

            public void setOptionId(Long optionId) {
                this.optionId = optionId;
            }

            public String getAccount() {
                return account;
            }

            public void setAccount(String account) {
                this.account = account;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public Long getProjectId() {
                return projectId;
            }

            public void setProjectId(Long projectId) {
                this.projectId = projectId;
            }

            public Long getSceneId() {
                return sceneId;
            }

            public void setSceneId(Long sceneId) {
                this.sceneId = sceneId;
            }

            public Integer getVersion() {
                return version;
            }

            public void setVersion(Integer version) {
                this.version = version;
            }

            public Integer getLodLevel() {
                return lodLevel;
            }

            public void setLodLevel(Integer lodLevel) {
                this.lodLevel = lodLevel;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public boolean isAddMode() {
                return addMode;
            }

            public void setAddMode(boolean addMode) {
                this.addMode = addMode;
            }

            public boolean isUploadProperty() {
                return uploadProperty;
            }

            public void setUploadProperty(boolean uploadProperty) {
                this.uploadProperty = uploadProperty;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }

    }
}
