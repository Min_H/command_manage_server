package com.suitbim.commandserver.commandserver.client;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.File;
import java.util.concurrent.ThreadPoolExecutor;

public class NavisworksCommandClient extends AbstractClient {

    @Value("${mode-read-application.navisworks.path}")
    private String navisworksPath;

    @Value("${mode-read-application.navisworks.inputPath}")
    private String navisworksInputPath;

    public NavisworksCommandClient(Gson gson, ThreadPoolExecutor threadPoolExecutor, RedisTemplate<String, String> redisTemplate, String taskQueueTopic) {
        super(gson, threadPoolExecutor, redisTemplate, taskQueueTopic);
    }

    @Override
    public String getClientPath() {
        return navisworksPath;
    }

    @Override
    public String getInputPath() {
        return navisworksInputPath;
    }

    @Override
    public File currentFile(Command command) {
        String[] split = command.getUrl().split("/");
        String fileName = split[split.length - 1];
        File file = new File(getInputPath() + "/" + fileName);

        return file;
    }

    @Override
    public String buildCommand(Command command) {
        return null;
    }

    @Override
    public boolean execute(Command command) {
        return false;
    }
}
