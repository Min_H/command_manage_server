package com.suitbim.commandserver.commandserver.client;

import com.google.gson.Gson;
import com.suitbim.commandserver.commandserver.util.HttpClientUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.*;
import java.util.concurrent.ThreadPoolExecutor;

public class Cesium3DTilesConvertClient extends AbstractClient {
    public Cesium3DTilesConvertClient(Gson gson, ThreadPoolExecutor threadPoolExecutor, RedisTemplate<String, String> redisTemplate, String taskQueueTopic) {
        super(gson, threadPoolExecutor, redisTemplate, taskQueueTopic);
    }

    @Autowired
    private HttpClientUtil httpClientUtil;

    @Value("${redis-message-queue.threeD-tiles-convert-command}")
    private String threeDTilesConvertQueue;

    @Value("${mode-read-application.threeD-tiles-convert.path}")
    private String threeDTilesConvertPath;

    @Override
    public String getClientPath() {
        return threeDTilesConvertPath;
    }

    @Override
    public String getInputPath() {
        return null;
    }

    @Override
    public File currentFile(Command command) {
        return null;
    }

    @Override
    public String buildCommand(Command command) {
//        return threeDTilesConvertPath
//                + "?sceneId=" + command.getSceneId()
//                + "&version=" + command.getParam().getVersion()
//                + "&lodLevel=" + command.getParam().getLodLevel();
        return threeDTilesConvertPath
                + " -i " + command.getSceneId()
                + " -v " + command.getParam().getVersion()
                + " -l " + command.getParam().getLodLevel();
    }

    @Override
    public boolean execute(Command command) {
//        String url = buildCommand(command);
//        try {
//            HttpClientUtil.RequestBuilder requestBuilder = httpClientUtil.buildRequest().buildGet(new URI(url));
//            HttpResponse response = requestBuilder.getResponse();
//            log.info("send request: " + url + " : " + response.getStatusLine().getStatusCode());
//        } catch (URISyntaxException | IOException e) {
//            e.printStackTrace();
//        }
//        return true;

        String cmd = buildCommand(command);
        Runtime runtime = Runtime.getRuntime();

        log.info("run command:" + cmd);
        try {
            Process process = runtime.exec(cmd);

            InputStream inputStream = process.getInputStream();
            InputStream errorStream = process.getErrorStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            InputStreamReader errorStreamReader = new InputStreamReader(errorStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            BufferedReader errorReader = new BufferedReader(errorStreamReader);
            String print;

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        while (errorReader.readLine() != null) {
                        }
                        errorReader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };

            threadPoolExecutor.execute(runnable);

            while (process.isAlive()) {
                print = bufferedReader.readLine();
                if (print == null) continue;
                else {
                    log.info("client print:" + print);
                }
            }
            bufferedReader.close();
            process.destroy();
            log.info("client execute finish");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
