package com.suitbim.commandserver.commandserver.control;

import com.suitbim.commandserver.commandserver.client.Cesium3DTilesConvertClient;
import com.suitbim.commandserver.commandserver.client.ifs.CommandClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/command")
@Component
public class CommandController {
    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping("/bimrun23dtilesForST")
    public String Bimrun23dtilesForST(Long sceneId, Integer version, Integer lodLevel) {
        CommandClient.Command command = new CommandClient.Command();
        CommandClient.Command.Param param = command.getParam();
        param.setSceneId(sceneId);
        param.setVersion(version);
        param.setLodLevel(lodLevel);

        Cesium3DTilesConvertClient cesium3DTilesConvertClient = applicationContext.getBean(Cesium3DTilesConvertClient.class);
        boolean success = cesium3DTilesConvertClient.execute(command);
        if (success) return "success";
        else return "fail";
    }
}
