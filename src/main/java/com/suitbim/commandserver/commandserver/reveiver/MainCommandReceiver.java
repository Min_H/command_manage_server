package com.suitbim.commandserver.commandserver.reveiver;

import com.google.gson.Gson;
import com.suitbim.commandserver.commandserver.client.ifs.CommandClient;
import com.suitbim.commandserver.commandserver.manage.FileManage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;

/**
 * 这中方式的监听所有的客户端都会收到，不能实现横向的任务处理扩展
 */

@Deprecated
public class MainCommandReceiver implements MessageListener {
    @Autowired
    private Gson gson;

    @Autowired
    private FileManage fileManage;

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void onMessage(Message message, byte[] bytes) {
        String channel = new String(message.getChannel());
        String body = new String(message.getBody());

        log.info("receive command:" + body);

        CommandClient.Command command = gson.fromJson(body, CommandClient.Command.class);

        try {
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            log.info("handle command finish");
        }
    }

}
