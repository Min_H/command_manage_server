package com.suitbim.commandserver.commandserver;

import com.suitbim.commandserver.commandserver.manage.CommandManage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

@SpringBootApplication
public class CommandServerApplication implements ApplicationListener<ContextRefreshedEvent> {


    public static void main(String[] args) {
        SpringApplication.run(CommandServerApplication.class, args);
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        CommandManage commandManage = contextRefreshedEvent.getApplicationContext().getBean(CommandManage.class);
        commandManage.init();
    }


}
