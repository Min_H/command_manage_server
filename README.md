# command_manage_server

#### 介绍
使用redis消息队列和http请求的开发的本地任务脚本调度服务。
这个项目的代码不多，有些代码也比较ugly，但是多数的内网本地服务调度都能用这个解决。
不同的脚本只需要按模式扩展就可以。
发布这个项目希望各位有相关功能需求时能有很方便地用一个套路解决解决完成。


#### 软件架构

springboot，redis，线程池调度



#### 安装教程



#### 使用说明

1. 安装java
2. 修改外置配置文件application.properties
3. 运行start.bat